close all; clear all; clc;
%% Author: J. Tachella (tachella.github.io) 2019
% This script loads the estimated background levels from the binary file
% (.bkg extension) into a MATLAB variable called 'bkg' of size Nr x Nc x L

%% Choose dataset (results have to be readily available) and frame number (in case of a 3D video)
filename = '1_mannequin_head';
frame = 0;

%% 
id =fopen(['output_'  filename '/background' num2str(frame) '.bkg']);
L = fread(id,1,'uint16');

if L>10
    Nrow = L;
    L =1;
else
    Nrow = fread(id,1,'uint16');
end

Ncol = fread(id,1,'uint16');

bkg = zeros(Nrow,Ncol,L);

for l=1:L
    for i=1:Nrow
        for j=1:Ncol
            bkg(i,j,l)= fread(id,1,'float');
        end
    end
end

fclose(id);

%% Show background image
if L==1
    imagesc(bkg)
end

