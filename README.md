## RT3D algorithm (Windows executable file)

**Main paper:** 
J. Tachella, Y. Altmann, N. Mellado, R. Tobin, A. McCarthy, G.S. Buller, J-Y. Tourneret and S. McLaughlin, "Real-time 3D reconstruction from single-photon lidar data using plug-and-play point cloud denoisers", Nature Communications, vol. 10, pp. 4984, November 2019
https://www.nature.com/articles/s41467-019-12943-7


**Multispectral extension (conference paper):** 
J. Tachella, Y. Altmann, J-Y. Tourneret and S. McLaughlin, "Real-time color 3D reconstruction from single-photon lidar data'', in Proc. International Workshop on Computational Advances in Multi Sensor Adaptive Processing (CAMSAP), Guadaloupe, West Indies, December 2019

## How to run this demo
1. Download the files in this repository
2. Check minimal requirements (below)
3. Run RT3D.exe
4. Follow the instructions in the command line. 
5. Use the mouse to change the viewpoint of the 3D reconstructions
6. The reconstructions are also saved in standard .ply format in a folder named "output_datasetX"
7. The background levels can be loaded into matlab from the .bkg file using the 'import_background_levels.m MATLAB script

If another dataset is desired, just uncomment the desired dataset in run_example

## Trying the code with your data
Add to the folder 'data' a my_data.mat file containing:
1. A MATLAB array Y (double) containing the Lidar histograms of size(Y) = [Nr, Nc, T, frames] where Nr and Nc are the number of vertical and horizontal pixels respectively, T is the number of histogram bins, and frames is the number of video frames (this dimension can be ignored if the dataset contains only a single frame).
2. A vector h (double) containing the system's IRF of size(h) = [1,T] the IRF is the same across pixels, or size(h) = [Nr, Nc,T] if the IRF changes across pixels. T can be smaller than the number of histogram bins if the IRF is approximately compactly supported (i.e. zero at the tails). 
3. A scalar scale_ratio (double) containing the ratio between the approximate size of a pixel and the size of a histogram bin (simplified camera parameters). WARNING: If this value is not correctly set, the algorithm won't provide meaningful results.

Run the script convert_file.m, selecting the file my_data.mat

Run RT3D.exe and select my_data.rbin dataset. Set the hyperparameters to obtain the best results (guidelines provided in the Supplementary Note 4 of the paper)

## Princeton Lightwave data
This executable file also is able to process raw Princeton Lightwave data, but needs a proper calibration if the user wants to test their own Princeton Lightwave data.
1. Run the MATLAB script plightwave_IRF.m using a plightwave_IRF.mat file containing a vector h with the pixelwise impulse response, sizeof(h) = [32,32,T], where T has to be smaller or equal than the number of histogram bins.
2. Copy your Princeton Lightwave data in the folder datasets. The default frame rate is 50.
2. Run RT3D.exe and select the proper .bin dataset. Set the hyperparameters to obtain the best results (guidelines provided in the Supplementary Note 4 of the paper). WARNING: It is very important to set the scale_ratio manually  (see above 'Trying the code with your data').

## Color 3D reconstruction 
A color reconstruction is available for 4 wavelengths (RGBY), using a coded aperture that measures only 1 wavelength per pixel (see the conference paper "Real-time color 3D reconstruction from single-photon lidar data" for further details).
Run datasets in the MSL_datasets folder using the bilateral RT3D algorithm (chosen by default) to evaluate the multispectral algorithm. In this case, a .ply file is saved per reconstructed wavelength. There is no support for running your own MSL datasets in this version (work in progress).

## Requirements
Windows 10 (not tested in other versions)

NVIDIA GPU with compute capability 5.0 or higher

Up-to-date NVIDIA drivers [download here](http://www.nvidia.com/drivers).

## Code
The codes used to generate this executable can be found in [this other repository](https://gitlab.com/Tachella/real-time-sp-lidar).

## Additional information
1. Parallel Cross-correlation and thresholded Cross-correlation are also included in the executable.
Please check that the parameters are correct (specially the subsampling parameter of cross-correlation)
2. The user can modify the hyperparameters of the algorithm by writing 'y' after selecting RT3D (follow the command line). The last choice of hyperparameters is automatically saved in the 'hyperparams' folder.
3. The .dll libraries are necessary for the program to work, do not delete or move them.
4. When only 1 frame is processed, the algorithm will run 3 times to warm up the GPU, before providing the final results.

